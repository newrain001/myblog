FROM python:3.10.5

LABEL MAINTAINER="XIAOMING"
# scp ./* 容器:/webapp/
COPY . /webapp

# cd /webapp
WORKDIR /webapp

RUN pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple
EXPOSE 8000
COPY ./docker-entrypoint.sh /
CMD ["sh", "/docker-entrypoint.sh" ]
