echo "启动成功"
sleep 10
mkdir -p upload/images
python manage.py migrate
python manage.py collectstatic
gunicorn --workers 8 \
--bind 0.0.0.0:8000 \
djangoProject.wsgi:application \
--log-level info \
--access-logfile /var/log/myblog.log \
--timeout 3600