from django import template
import json
register = template.Library()

@register.filter(name='setjson', is_safe=True)
def setjson(value, tags):
    data = eval(value)
    return data[tags]

@register.filter(name='checkover', is_safe=False)
def checkover(value, tags):
    data = eval(value)
    for i in data['userlist']:
        if data['userlist'][i]['status'] == tags:
            return '未完成'
    else:
        return '已完成'

@register.filter(name='dict_get', is_safe=False)
def dict_get(value, key):
    return value.get(key)

@register.filter(name='quyu', is_safe=False)
def quyu(value, key):
    if value % key == 0:
        return True
    else:
        return False