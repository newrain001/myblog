import mimetypes

from django.contrib import messages
from django.contrib.auth import authenticate
from django.db.models import F, QuerySet, Count
from django.http import JsonResponse, StreamingHttpResponse, FileResponse
from django.shortcuts import HttpResponse, render, redirect, reverse
from django.contrib.auth.decorators import login_required
# from django.views.decorators.cache import cache_page
from django.core.cache import cache
from django.conf import settings
from .forms import *
from .models import *
from .qingcloud import putImage
import datetime, requests, json, re, os
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_sameorigin
from .csdn import get_image
from wsgiref.util import FileWrapper
from urllib.parse import quote

#@cache_page(60)
def index(request):
    name = None
    if request.user.is_authenticated:
        name = User.last_name
    data = cache.get('csdn')
    if data is None:
        with open('App/csdn.json', 'r', encoding='utf-8') as f:
            data = json.load(f)
    return render(request, 'index.html', {'name': name, 'data': data})


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('loginName')
            password = form.cleaned_data.get('loginPass')
            user = authenticate(username=username, password=password)
            auth.login(request, user)
            info = getAddr(request.META['HTTP_X_FORWARDED_FOR']) if 'HTTP_X_FORWARDED_FOR' in request.META else getAddr(request.META['REMOTE_ADDR'])
            if info['status'] == 'success':
                Login.objects.create(username=username, client_addr=info['query'], city=info['city'],
                                     country=info['country'], region=info['regionName'], latitude=info['lat'],
                                     longitude=info['lon']).save()
            else:
                Login.objects.create(username=username, client_addr=request.META['REMOTE_ADDR']).save()
            url = request.GET.get('next', '/')
            return redirect(url)
        else:
            return render(request, 'login.html', {'errors': form.errors})
    else:
        return render(request, 'login.html')

@login_required
def userinfo(request):
    login_data = Login.objects.filter(username=request.user.username).order_by('-login_time')[:5]
    image_data = Upload.objects.filter(username=request.user.username).order_by('-upload_time')[:5]
    blog_data = paper.objects.filter(username=request.user.username).order_by('-paper_time')[:5]
    task_data = Task.objects.filter(username=request.user.username).order_by('-task_time')[:5]
    video_data = videoUpload.objects.filter(username=request.user.username).order_by('-video_time')[:5]
    package_data = packageUpload.objects.filter(username=request.user.username).order_by('-package_time')[:5]
    return render(request, 'function/user.html', {'name': auth.get_user(request).username, 'login_data': login_data, 'image_data': image_data, 'blog_data': blog_data, 'task_data': task_data, 'video_data': video_data, 'package_data': package_data, 'regtime': auth.get_user(request).date_joined})


def logout(request):
    auth.logout(request)
    return redirect(reverse('App:index'))

@login_required
def repassword(request):
    return HttpResponse('功能尚未支持，修改密码请联系管理员')


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data.get('fname')
            last_name = form.cleaned_data.get('lname')
            username = form.cleaned_data.get('username')
            email = request.POST.get('email')
            password = form.cleaned_data.get('password')
            likes = request.POST.getlist('like')
            role = "False"
            try:
                User.objects.create_user(username=username, email=email, password=password, first_name=first_name,
                                         last_name=last_name, is_staff=role, likes=likes)
            except Exception as e:
                return render(request, 'register.html', {'errors': e})
            return redirect(reverse('App:login'))
        else:
            return render(request, 'register.html', {'errors': form.errors})
    else:
        return render(request, 'register.html')


#@cache_page(60)
@login_required
def dashboard(request, type):
    username = auth.get_user(request).username
    if request.GET.get('time') == 'week':
        t = 6
    elif request.GET.get('time') == 'month':
        t = 29
    elif request.GET.get('time') == 'year':
        t = 364
    elif request.GET.get('time') == 'quarter':
        t = 90
    else:
        t = 6
    if type == 'login':
        data = getData(username, t)
        table = Login.objects.order_by('-login_time').all()[:30]
    elif type == 'docs':
        data = getData(username, t, 'docs')
        table = Upload.objects.filter(username=username).order_by('-upload_time').all()[:100]
    elif type == 'blog':
        data = getData(username, t, 'blog')
        table = paper.objects.order_by('-paper_time').all()[:100]
    elif type == 'video':
        data = getData(username, t, 'video')
        table = videoUpload.objects.order_by('-video_time').all()[:100]
    elif type == 'package':
        data = getData(username, t, 'package')
        table = packageUpload.objects.order_by('package_time').all()[:100]
    else:
        data = table = ""
    return render(request, 'nav/dashboard.html', {'data': data, 'table': table, 'type': type})

@xframe_options_sameorigin
@csrf_exempt
@login_required
def upload(request):
    ftype = request.GET.get('type')
    if request.method == 'POST':
        if ftype == 'videos':
            file = request.FILES.getlist('file', None)
            data = {'status': 'success', 'msg': []}
            today = str(datetime.date.today())
            file_path = os.path.join(settings.MEDIA_ROOT, 'videos',today)
            for i in file:
                if '@' in i.name:
                    file_type = i.name.split('@')[0]
                    i.name = i.name.split('@')[1]
                else:
                    file_type = 'other'
                file_path = os.path.join(file_path, file_type)
                if not os.path.exists(file_path):
                    os.makedirs(file_path)
                file_path = os.path.join(file_path, i.name)
                video = videoUpload.objects.filter(video_title=i.name,video_size=i.size)
                if video.exists():
                    import shutil
                    shutil.move(os.path.join(settings.MEDIA_ROOT,str(video.get().video_path).replace('/upload/','')), file_path)
                    video.update(video_time=today, video_path=os.path.join('/upload/videos/',file_path.split(r"videos")[-1][1:].replace('\\','/')))
                else:
                    image = get_image('215x280', file_type)
                    try:
                        videoUpload.objects.create(username=auth.get_user(request).username, video_title=i.name, video_size=i.size, video_time=today, video_path=os.path.join('/upload/videos/',file_path.split(r"videos")[-1][1:].replace('\\','/')), video_image=image, video_desc=file_type)
                        with open(file_path, 'wb') as f:
                            for chunk in i.chunks():
                                f.write(chunk)
                    except Exception as e:
                        print(e)

            if data:
                return HttpResponse(json.dumps(data))
        elif ftype == 'editmd':
            file = request.FILES.get('editormd-image-file')
            url_path = putImage(file)
            if Upload.objects.filter(username=auth.get_user(request).username, file_name=file.name,file_size=file.size).exists():
                Upload.objects.filter(file_name=file.name, file_size=file.size).update(upload_time=datetime.datetime.now())
            else:
                Upload.objects.create(username=auth.get_user(request).username, file_name=file.name, file_path=file.name,file_size=file.size, url_path=url_path['url']).save()
            data = {'success': 1, 'message': "上传成功", 'url': url_path['url']}
            return JsonResponse(data)
        elif ftype == 'package':
            file = request.FILES.getlist('file', None)
            data = {'status': 'success', 'msg': []}
            file_path = os.path.join(settings.MEDIA_ROOT, 'package')
            for i in file:
                if not os.path.exists(file_path):
                    os.makedirs(file_path)
                file_path = os.path.join(file_path, i.name)
                package = packageUpload.objects.filter(username=auth.get_user(request).username,package_title=i.name,package_size=i.size)
                if package.exists():
                    package.update(username=auth.get_user(request).username)
                else:
                    try:
                        packageUpload.objects.create(username=auth.get_user(request).username, package_title=i.name, package_size=i.size).save()
                        with open(file_path, 'wb') as f:
                            for chunk in i.chunks():
                                f.write(chunk)
                    except Exception as e:
                        print(e)
            return JsonResponse(data)
        else:
            file = request.FILES.getlist('file', None)
            data = {'status': 'success', 'msg': []}
            for i in file:
                if Upload.objects.filter(username=auth.get_user(request).username,file_name=i.name,file_size=i.size).exists():
                    Upload.objects.filter(file_name=i.name,file_size=i.size).update(upload_time=datetime.datetime.now())
                else:
                    url_path = putImage(i)
                    data['msg'].append(url_path)
                    Upload.objects.create(username=auth.get_user(request).username, file_name=i.name, file_path=i.name, file_size=i.size, url_path=url_path['url']).save()
            if data:
                return HttpResponse(json.dumps(data))
    return render(request, 'nav/upload.html', {'type': ftype})

# 功能函数
def getData(username, time, type='login'):
    data = {}
    for i in range(time, -1, -1):
        date = datetime.date.today() - datetime.timedelta(days=i)
        if type == 'login':
            data.update({str(date): Login.objects.filter(username=username, login_time__startswith=date).count()})
        elif type == 'docs':
            data.update({str(date): Upload.objects.filter(username=username, upload_time__startswith=date).count()})
        elif type == 'blog':
            data.update({str(date): paper.objects.filter(paper_time__startswith=date).count()})
        elif type == 'video':
            data.update({str(date): videoUpload.objects.filter(video_time__startswith=date).count()})
        elif type == 'package':
            data.update({str(date): packageUpload.objects.filter(package_time__startswith=date).count()})
    return data


def getAddr(ip=None):
    info = requests.get('http://ip-api.com/json/' + ip + '?lang=zh-CN')
    if info:
        return info.json()
    else:
        info = {
            "status": "success",
            "query": "none",
            "city": "none",
            "country": "none",
            "regionName": "none",
            "lat": "none",
            "lon": "none"
        }
        return info

@login_required
def listImage(request):
    image = Upload.objects.filter(username=auth.get_user(request).username).all().order_by('-upload_time')
    return render(request, 'nav/image.html', {'image': image})

@login_required
def listTask(request):
    task = Task.objects.all()
    return render(request, 'task/tasklist.html', {'task': task})

def addTask(request):
    if request.method == 'POST':
        task_title = request.POST.get('title')
        image = request.POST.get('bimage')
        if not image :
            image = 'https://py-put.gd2.qingstor.com/0bb68da4b7edd5ad21bee1f9ece0bdca5871f5a9.jpg'
        data = {}
        now = datetime.datetime.now()
        data.update({'backend_image': image, 'userlist': {}.fromkeys(request.POST.get('userlist').split(','),{'status':'未完成','update_time': str(now)}), 'desc': request.POST.get('desc')})
        Task.objects.create(username=auth.get_user(request).username,task_title=task_title, task_content=json.dumps(data, ensure_ascii=False),).save()
        return redirect(reverse('App:listTask'), {'messages': task_title + '添加成功'})
    return render(request, 'task/taskcreate.html')

@login_required
def checkTask(request, id):
    data = Task.objects.filter(id=id).get()
    userdata = json.loads(data.task_content)
    if request.method == 'POST':
        url = request.get_raw_uri().split('?')[0]
        if eval(request.COOKIES.get(f'sign{id}', '{}')).get('url') != url:
            task = Task.objects.get(id=id)
            jsondata = json.loads(task.task_content)
            jsondata['userlist'][request.GET.get('name')] = {'status': '已完成', 'update_time': str(datetime.datetime.now())}
            Task.objects.filter(id=id).update(task_content=json.dumps(jsondata, ensure_ascii=False))
        obj = redirect(reverse('App:checkTask', kwargs={'id': id}))
        obj.set_cookie(f'sign{id}', {'url': request.get_raw_uri().split('?')[0], 'sign_num': eval(request.COOKIES.get(f'sign{id}','{}')).get('sign_num',0) + 1}, expires=60 * 60 * 24)
        return obj
    return render(request, 'task/detailed.html', {'data': userdata['userlist'],'id':id})

@csrf_exempt
@login_required
def editMD(request, id=None):
    if request.method == 'POST':
        title = request.POST.get('title')
        desc = request.POST.get('desc')
        text = request.POST.get('test-editormd-markdown-doc')
        image = get_image('200x300')
        if paper.objects.filter(username=auth.get_user(request).username, paper_title=title).exists():
            paper.objects.filter(username=auth.get_user(request).username,paper_title=title).update(paper_desc=desc, paper_content=text, paper_time=datetime.datetime.now())
        else:
            paper.objects.create(username=auth.get_user(request).username, paper_title=title, paper_desc=desc,
                                 paper_content=text, paper_image=image).save()
        return redirect(reverse('App:paperList'))
    else:
        data = {}
        if f'{id}'.isdigit():
            data = paper.objects.filter(id=id).get()
        return render(request, 'function/edit.html', {'data': data})

def paperList(request):
    data = paper.objects.all().order_by('-paper_time')
    thumbs_up = {}
    for i in data:
        thumbs_up[i.id] = thumbs.objects.filter(paper_id=i.id).count()
    return render(request, 'nav/paperlist.html', {'data': data, 'thumbs': thumbs_up, 'username': auth.get_user(request).username})

def paperDetail(request, id):
    if request.method == 'POST':
        if thumbs.objects.filter(username=auth.get_user(request).username, paper_id=id).exists():
            thumbs.objects.filter(username=auth.get_user(request).username, paper_id=id).delete()
        else:
            thumbs.objects.create(username=auth.get_user(request).username, paper_id=paper.objects.filter(id=id).get()).save()
        return redirect(reverse('App:paperDetail', kwargs={'id': id}))
    data = paper.objects.filter(id=id).get()
    if data:
        paper.objects.filter(id=id).update(views=paper.objects.filter(id=id).get().views+1)
    return render(request, 'function/viewmd.html', {'data': data, 'thumbs': thumbs.objects.filter(paper_id=id).count()})

def videoView(request, id):
    if request.method == 'GET':
        data = videoUpload.objects.filter(id=id).get()
        return render(request, 'nav/videoview.html', {'path': data.video_path})

def videoList(request):
    video_type = request.GET.get('video_type', '全部内容')
    if video_type == '全部内容':
        data = videoUpload.objects.all().order_by('video_title', '-video_time')
    else:
        data = videoUpload.objects.filter(video_desc=video_type).all().order_by('video_title','-video_time')
    desc = videoUpload.objects.all().values('video_desc').distinct()
    desc = [i['video_desc'] for i in desc]
    desc.append('全部内容')
    return render(request, 'nav/videolist.html', {'data': data, 'desc': desc, 'selected': video_type})

def file_iterator(file_name, chunk_size=8192, offset=0, length=None):
    with open(file_name, "rb") as f:
        f.seek(offset, os.SEEK_SET)
        remaining = length
        while True:
            bytes_length = chunk_size if remaining is None else min(remaining, chunk_size)
            data = f.read(bytes_length)
            if not data:
                break
            if remaining:
                remaining -= len(data)
            yield data

def stream_video(request):
    """将视频文件以流媒体的方式响应"""
    range_header = request.META.get('HTTP_RANGE', '').strip()
    range_re = re.compile(r'bytes\s*=\s*(\d+)\s*-\s*(\d*)', re.I)
    range_match = range_re.match(range_header)
    path = request.GET.get('path')
  #这里根据实际情况改变，我的views.py在core文件夹下但是folder_path却只到core的上一层，media也在core文件夹下
    folder_path = os.getcwd().replace('\\', '/')
    path=folder_path+path #path就是template ？后面的参数的值
    size = os.path.getsize(path)
    content_type, encoding = mimetypes.guess_type(path)
    content_type = content_type or 'application/octet-stream'
    if range_match:
        first_byte, last_byte = range_match.groups()
        first_byte = int(first_byte) if first_byte else 0
        last_byte = first_byte + 1024 * 1024 * 10
        if last_byte >= size:
            last_byte = size - 1
        length = last_byte - first_byte + 1
        resp = StreamingHttpResponse(file_iterator(path, offset=first_byte, length=length), status=206, content_type=content_type)
        resp['Content-Length'] = str(length)
        resp['Content-Range'] = 'bytes %s-%s/%s' % (first_byte, last_byte, size)
    else:
        resp = StreamingHttpResponse(FileWrapper(open(path, 'rb')), content_type=content_type)
        resp['Content-Length'] = str(size)
    resp['Accept-Ranges'] = 'bytes'
    return resp

def videoDownload(request, id):
    data = videoUpload.objects.filter(id=id).get()
    file_path = str(data.video_path)[1:]
    file_name = os.path.basename(file_path)
    file = open(file_path, 'rb')
    response = FileResponse(file)
    response['Content-Type'] = 'application/octet-stream'
    response['Content-Disposition'] = 'attachment;filename="{}"'.format(quote(file_name))
    return response

def packagelist(request):
    data = packageUpload.objects.all().order_by('package_time')
    return render(request, 'nav/packagelist.html', {'data': data})

def packageDownload(request, id):
    try:
        data = packageUpload.objects.filter(id=id)
        data.update(downloads=data.get().downloads+1)
        file_path = os.path.join(settings.MEDIA_ROOT, 'package', data.get().package_title)
        file_name = os.path.basename(file_path)
        file = open(file_path, 'rb')
        response = FileResponse(file)
        response['Content-Type'] = 'application/octet-stream'
        response['Content-Disposition'] = 'attachment;filename="{}"'.format(quote(file_name))
        return response
    except Exception as e:
        print(e)
        return HttpResponse('内部错误')