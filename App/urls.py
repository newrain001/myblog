from django.urls import path
from App import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('userinfo/', views.userinfo, name='userinfo'),
    path('repassword/', views.repassword, name='repassword'),
    path('logout/', views.logout, name='logout'),
    path('dashboard/<type>', views.dashboard, name='dashboard'),
    path('upload/', views.upload, name='upload'),
    path('image/', views.listImage, name='image'),
    path('tasklist/', views.listTask, name='listTask'),
    path('taskadd/', views.addTask, name='addTask'),
    path('taskcheck/<id>', views.checkTask, name='checkTask'),
    path('edit/<id>', views.editMD, name='editMD'),
    path('paperlist/', views.paperList, name='paperList'),
    path('paperview/<id>', views.paperDetail, name='paperDetail'),
    path('video/<id>', views.videoView, name='videoView'),
    path('videolist/', views.videoList, name='videoList'),
    # static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    path('videoView/', views.stream_video, name='stream_video'),
    path('videoDownload/<id>', views.videoDownload, name='videoDownload'),
    path('packagelist/', views.packagelist, name='packagelist'),
    path('packageDownload/<id>', views.packageDownload, name='packageDownload'),
]
