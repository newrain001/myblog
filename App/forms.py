from django import forms

from django.contrib import auth
from .models import User


# 表单类：注册表单
class RegisterForm(forms.Form):
    # 对每一个输入框验证
    fname = forms.CharField(
        max_length=20,  # 最大长度
        error_messages={
            'max_length': '长度不能超过20',
        }
    )
    lname = forms.CharField(
        max_length=20,  # 最大长度
        error_messages={
            'max_length': '长度不能超过20',
        }
    )
    username = forms.CharField(
        max_length=20,  # 最大长度
        error_messages={
            'max_length': '长度不能超过20',
        }
    )
    password = forms.CharField(
        max_length=18,  # 最大长度
        min_length=6,  # 最小长度
        error_messages={
            'max_length': '长度不能超过18',
            'min_length': '长度不能小于6'
        }
    )


# 表单类：登录表单
class LoginForm(forms.Form):
    # 重写clean方法
    loginName = forms.CharField(
        max_length=20,  # 最大长度
        error_messages={
            'max_length': '长度不能超过20',
        }
    )
    loginPass = forms.CharField(
        max_length=20,  # 最大长度
        error_messages={
            'max_length': '长度不能超过20',
        }
    )
    def clean(self):
        # 获取清洗之后的数据
        loginName = self.cleaned_data.get('loginName')
        loginPass = self.cleaned_data.get('loginPass')
        # 检测用户名是否存在
        if not User.objects.filter(username=loginName).exists():
            raise forms.ValidationError({'loginName': '用户名不存在'})

        # 验证用户名和密码是否匹配
        if not auth.authenticate(username=loginName, password=loginPass):
            raise forms.ValidationError({'loginPass': '密码错误'})

        return self.cleaned_data


