from qingstor.sdk.service.qingstor import QingStor
from qingstor.sdk.config import Config
import hashlib,os
from django.conf import settings

config = Config('CEWTHUPRVWVSARKQXBMV', 'mlGCl7V7s7ZbTmUjwV6NFCbpRNojBfV0nUYj2Sym')
qingstor = QingStor(config)


def hashfile(file):
    with open(file, 'rb') as f:
        s = hashlib.sha1(f.read())
    return s.hexdigest()


def putImage(file):
    file_name = file.name
    file_size = file.size
    file_path = os.path.abspath(os.path.join(settings.MEDIA_ROOT,'images', file_name))
    with open(file_path , 'wb') as f:
        for chunk in file.chunks():
            f.write(chunk)
    s = hashfile(file_path)
    bucket = qingstor.Bucket('py-put', 'gd2')
    filetype = os.path.basename(file_path).split('.')[-1]
    with open(file_path, 'rb') as f:
        output = bucket.put_object(
            s +'.'+ filetype, body=f
        )

    # Print the HTTP status code.
    # Example: 201'
    if output.status_code == 201:
        print('Uploaded successfully.')
        return {'url': f'https://py-put.gd2.qingstor.com/{s}.{filetype}', 'urlname': s +'.'+ filetype, 'filesize': file_size, 'filename': file_name, 'hash': s}
    else:
        print('Upload failed.')
        return False