from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    likes = models.CharField(max_length=100)

class Login(models.Model):
    username = models.CharField(max_length=100)
    login_time = models.DateTimeField(auto_now_add=True)
    client_addr = models.CharField(max_length=100)
    country = models.CharField(max_length=100)
    region = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    latitude = models.CharField(max_length=100)
    longitude = models.CharField(max_length=100)
    def __str__(self):
        return self.username

class Upload(models.Model):
    username = models.CharField(max_length=100)
    upload_time = models.DateTimeField(auto_now_add=True)
    file_name = models.CharField(max_length=100)
    file_size = models.IntegerField(default=0)
    file_path = models.FileField(upload_to='uploads/')
    url_path = models.CharField(max_length=200, default='none')
    def __str__(self):
        return self.username

class Task(models.Model):
    username = models.CharField(max_length=100)
    task_time = models.DateTimeField(auto_now_add=True)
    task_title = models.CharField(max_length=100)
    task_status = models.BooleanField(default=False)
    task_content = models.JSONField(max_length=10000)
    def __str__(self):
        return self.username

class paper(models.Model):
    username = models.CharField(max_length=100)
    paper_time = models.DateTimeField(auto_now_add=True)
    paper_title = models.CharField(max_length=200)
    paper_desc = models.CharField(max_length=2000)
    paper_content = models.TextField()
    views = models.IntegerField(default=0)
    paper_image = models.URLField(max_length=10000, default='')
    def __str__(self):
        return self.username

class comment(models.Model):
    username = models.CharField(max_length=100)
    comment_time = models.DateTimeField(auto_now_add=True)
    comment_content = models.CharField(max_length=100)
    paper_id = models.ForeignKey(paper, on_delete=models.CASCADE)
    def __str__(self):
        return self.username

class thumbs(models.Model):
    username = models.CharField(max_length=100)
    thumbs_time = models.DateTimeField(auto_now_add=True)
    paper_id = models.ForeignKey(paper, on_delete=models.CASCADE)
    def __str__(self):
        return self.username

class videoUpload(models.Model):
    username = models.CharField(max_length=100)
    video_time = models.DateField(auto_now_add=True)
    video_title = models.CharField(max_length=200, unique=True)
    video_size = models.IntegerField(default=0)
    video_path = models.FileField()
    video_desc = models.TextField(default="")
    views = models.IntegerField(default=0)
    video_image = models.URLField(max_length=10000, default='')
    def __str__(self):
        return self.username

class packageUpload(models.Model):
    username = models.CharField(max_length=100)
    package_time = models.DateTimeField(auto_now_add=True)
    package_title = models.CharField(max_length=200, unique=True)
    package_size = models.IntegerField(default=0)
    #package_desc = models.TextField(default="")
    downloads = models.IntegerField(default=0)
    def __str__(self):
        return self.username
