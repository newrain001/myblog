from django.utils.deprecation import MiddlewareMixin

# 配置用户注册时的中间件，用于检查用户输入内容是否合法
class RegMiddleware(MiddlewareMixin):
    def process_request(self, request):
        pass